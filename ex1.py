#Exercise 1 - Printing

print "Hello World!"
print "Hello Again"
print "I like typing like this"
print "This is fun"
print 'Yay! Printing'
print "I'd much rather you 'not'."
print 'I "said" do not touch this.'

#Study Drills - print another line 
print 'Another line printed'

