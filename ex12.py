age = raw_input ("How old are you? ")
height = raw_input ("How tall are you? ")
weight = raw_input ("How much do you weigh?")

print "So, you're %r old, %r tall, and %r heavy" %(age, height, weight)

# Remember, %s is for displaying string properly, %r is for raw debugging. 
# %r might have weird character escape?