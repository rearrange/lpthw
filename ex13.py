from sys import argv

script, first, second, third = argv

print "The script is called: ", script
print "Your first variable is: ", first
print "Your second variable is: ", second
print "Your third variable is: ", third

# Study Drills: Write a script that has fewer arguments and one that has more

# Fewer arguments (uncomment line 13 until 16)
#scriptname, only_one = argv

#print "The script is called: ", scriptname
#print "Your only variable: ", only_one

# More arguments (uncomment line 19 until 25)
#scriptname, not_only_one, two, three, fours = argv

#print "The script is called: ", scriptname
#print "Your not only variable: ", not_only_one
#print "Variable two: ", two
#print "Variable three: ", three
#print "Variable fours: ", fours

# Combine raw_input with argv to make a script that gets more input from user
# Uncomment all lines below, and uncomment any lines above
# Make sure to unpack the right line!

#others  = raw_input("Anything you wanna add? ")
#others2 = raw_input("That's all? ")
#print "Here's your raw input: %r , %r " %(others, others2)


