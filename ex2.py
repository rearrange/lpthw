# Exercise 2 - How To Use Comments
# A comment, this is so you can read your program later
# Anything after the # is ignored by Python

print "I could have code like this." #and the comment after the octothorpe is ignored

# You can also use a comment to "disable" a part of the code
# print "This won't run"

print "This will run."