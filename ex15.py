from sys import argv

# Set two variables to be unpack by argv (argument variables)
script, filename = argv

# Assign variable to open file
txt = open(filename)

print "Here's your file %r: " %filename
# Read content of file object txt
print txt.read()

# Redo via raw_input
print "Type the filename again:"
file_again = raw_input("> ")

txt_again = open(file_again)
print txt_again.read()

# Study drill 7 - Call close()
txt.close()
txt_again.close()

