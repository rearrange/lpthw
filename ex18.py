# Ex18: Functions!
# Functions do three things:
#   1. They name pieces of codes the way variables name 
#      strings and numbers
#   2. They take arguments the way your script take argv
#   3. Using 1 & 2 they let you make your own "mini
#      scripts" or "tiny command"

# This one is like your scripts with argv
def print_two(*args):
      arg1, arg2 = args
      print "arg1: %r, arg2: %r" % (arg1, arg2)
      
# That *args is actually pointless, we can just do this
def print_two_again(arg1, arg2):
      print "arg1: %r, arg2: %r" % (arg1, arg2)
      
# This just takes one arguments
def print_one(arg1):
      print "arg1: %r" % arg1
      
# This one takes no arguments
def print_none():
      print "I got nothing!"
      
print_two("rearrange", "inc")
print_two_again("Sallehin", "Sallehuddin")
print_one("First!")
print_none()